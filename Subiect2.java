import javax.swing.*;
import java.awt.*;

public class Subiect2 extends JFrame {
    private JTextArea jTextArea;
    private JTextArea jTextArea2;
    private String text = "AAAA";
    private int n = 0;

    public Subiect2() throws HeadlessException {
        setSize(700, 700);
        JPanel panel = new JPanel();
        panel.setLayout(null);

        jTextArea = new JTextArea("");
        jTextArea.setBounds(20, 10, 100, 50);
        jTextArea.setEditable(false);
        jTextArea2 = new JTextArea("");
        jTextArea2.setBounds(20, 70, 100, 50);
        jTextArea2.setEditable(false);
        panel.add(jTextArea);
        panel.add(jTextArea2);

        JButton button = new JButton("Buton");
        button.setBounds(100, 100, 100, 50);
        panel.add(button);

        button.addActionListener(e -> Button());

        add(panel);
        setVisible(true);
    }

    private void Button() {
        if (this.n == 0) {
            jTextArea.setText(this.text);
            this.n++;
        } else {
            if (n % 2 == 0) {
                jTextArea.setText("");
                jTextArea2.setText(this.text);
                n++;
            } else {
                jTextArea.setText(this.text);
                jTextArea2.setText("");
                n++;
            }

        }
    }

    public static void main(String[] args) {
        Subiect2 s = new Subiect2();
    }
}