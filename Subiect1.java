public class Subiect1 {
    public static void main(String[] args) {
        F f = new F();
        E e = new E(f);
        B b = new B(123,e);
    }
}

interface C {

}

interface D {
    void x();
}

class G {

}

class E extends G {
    private F f;
    private H h = new H();

    public E(F f) {
        this.f = f;
    }

    public void metG(int i) {

    }
}


class F {
    public void meatA() {

    }
}

class H {

}

class B implements C, D  {
    private long t;
    private E e;

    public B(long t, E e) {
        this.t = t;
        this.e = e;
    }

    @Override
    public void x() {

    }
}